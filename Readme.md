
# Set up

After cloning this, you'll need to "hook" the service and testing repos into the right places:

    cd service
    git clone ... iCMS-EPR
    cd ../test
    git clone ... icms-epr-test
    
Then go back to the top dir, build and start the test:

    cd ..
    docker-compose build 
    docker-compose up

