#!/usr/bin/env bash

# wait a bit for Xvfb to become ready ... 
sleep 10

echo "starting up the server ..."
/opt/bin/entry_point.sh > /data/logs/server.log 2>&1 &

echo "waiting a bit ... "
sleep 5

echo "starting tests ... "
python -m unittest discover -s ./app/tests/

# ... and exit