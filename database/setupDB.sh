#!/bin/bash
set -e

/etc/init.d/postgresql start
sleep 10
/etc/init.d/postgresql status
psql -f create_fixtures.sql    
/etc/init.d/postgresql stop
