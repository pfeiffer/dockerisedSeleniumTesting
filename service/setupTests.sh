#!/usr/bin/env bash

echo "waiting a bit ..."
sleep 5

export ICMS_EPR_TESTING=`hostname`

echo "preparing DB"
python /data/iCMS-EPR/manage.py shell <<EOF
import SetupDummies
SetupDummies.resetDB()
SetupDummies.setup()
EOF

echo "starting service"
python /data/iCMS-EPR/manage.py runserver -h 0.0.0.0 -p 5000
