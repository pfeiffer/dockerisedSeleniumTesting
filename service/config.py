
import os
import sys
import socket

basedir = os.path.abspath(os.path.dirname(__file__))

prodLevel = 'test'

class Config:

    SECRET_KEY = 'aReallySecretKeyForTestingOnly....'
    SSL_DISABLE = False

    host = socket.gethostname()

    print "found host : ", host
    print "      at IP: ", socket.gethostbyname(socket.gethostname())

    SERVER_NAME="192.168.64.5:5000"

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # uncomment the line below to see _all_ queries printed (and make sure
    # logging is off, otherwise you'll get everything double):
    # SQLALCHEMY_ECHO = True

    MAIL_SERVER = 'smtp.cern.ch'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    # MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    # MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    SECURITY_EMAIL_SENDER = 'andreas.pfeiffer@cern.ch'
    ICMS_MAIL_SUBJECT_PREFIX = '[iCMS-EPR]'
    ICMS_MAIL_SENDER = 'icms-support@cern.ch'
    ICMS_ADMIN = os.environ.get('iCMS_ADMIN')

    # do not intercept redirects in debug tool ... set
    # this to true if you do want the intercepts
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    ICMS_SLOW_DB_QUERY_TIME=0.5

    @staticmethod
    def init_app(app):
        pass

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'postgres://postgres@db:icms_epr_test'
    WTF_CSRF_ENABLED = False
    DEBUG = True

config = {
    'testing': TestingConfig,

    'default': TestingConfig
}
